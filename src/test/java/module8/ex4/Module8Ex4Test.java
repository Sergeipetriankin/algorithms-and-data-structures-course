package module8.ex4;

import org.junit.Before;
import org.junit.Test;

public class Module8Ex4Test {
    private char[] chars;

    @Before
    public void setUp() throws Exception {
        chars = new char[]{'a', 'b', 'c', 'd', 'e', 'f'};
    }

    @Test
    public void allSubsetsOfSet() {
        Module8Ex4.allSubsetsOfSet(chars);
    }
}