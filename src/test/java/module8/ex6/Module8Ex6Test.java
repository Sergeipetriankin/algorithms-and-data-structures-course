package module8.ex6;

import org.junit.Before;
import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.assertEquals;

public class Module8Ex6Test {

    private Stack<Integer> towerA;
    private Stack<Integer> controlStack;
    private Stack<Integer> towerB;
    private Stack<Integer> towerC;

    @Before
    public void setUp() throws Exception {
        towerA = new Stack<>();
        controlStack = new Stack<>();
        towerB = new Stack<>();
        towerC = new Stack<>();

        for (int i = 5; i >0; i--){
            towerA.push(i);
            controlStack.push(i);
        }
    }

    @Test
    public void towerOfHanoi() {
        Module8Ex6.towerOfHanoi(5, towerA, towerC, towerB);
        assertEquals(controlStack, towerC);

    }
}