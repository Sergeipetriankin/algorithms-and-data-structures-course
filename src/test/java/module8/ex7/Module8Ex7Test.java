package module8.ex7;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class Module8Ex7Test {

    private String string;
    private int numOfPerms;

    @Before
    public void setUp() throws Exception {
        string = "abcdefg";
        numOfPerms = 1;
        for (int i = 1; i <= string.length(); i++) {
            numOfPerms *= i;
        }
    }

    @Test
    public void permutation() {
        List<String> permutation = Module8Ex7.permutation(string);
        assertEquals(numOfPerms, permutation.size());
    }
}