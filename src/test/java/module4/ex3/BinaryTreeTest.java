package module4.ex3;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class BinaryTreeTest {

    private BinaryTree.Node<Integer> root;
    private BinaryTree tree;

    @Before
    public void setUp() throws Exception {
        root =  new BinaryTree.Node<>(0);
        BinaryTree.Node<Integer> node1 = new BinaryTree.Node<>(1);
        BinaryTree.Node<Integer> node2 = new BinaryTree.Node<>(2);
        BinaryTree.Node<Integer> node3 = new BinaryTree.Node<>(3);
        BinaryTree.Node<Integer> node4 = new BinaryTree.Node<>(4);
        BinaryTree.Node<Integer> node5 = new BinaryTree.Node<>(5);
        BinaryTree.Node<Integer> node6 = new BinaryTree.Node<>(6);
        BinaryTree.Node<Integer> node7 = new BinaryTree.Node<>(7);
        BinaryTree.Node<Integer> node8 = new BinaryTree.Node<>(8);
        BinaryTree.Node<Integer> node9 = new BinaryTree.Node<>(9);
        BinaryTree.Node<Integer> node10 = new BinaryTree.Node<>(10);

        root.setLeft(node1);
        root.setRight(node2);

        node1.setLeft(node3);

        node2.setLeft(node4);
        node2.setRight(node5);

        node3.setLeft(node6);
        node3.setRight(node7);

        node4.setRight(node8);

        node5.setLeft(node9);
        node5.setRight(node10);

        List<BinaryTree.Node> nodes = new ArrayList<>();
        nodes.add(node1);
        nodes.add(node2);
        nodes.add(node3);
        nodes.add(node4);
        nodes.add(node5);
        nodes.add(node6);
        nodes.add(node7);
        nodes.add(node8);
        nodes.add(node9);
        nodes.add(node10);
        tree = new BinaryTree(nodes);
        System.out.println();

    }

    @Test
    public void countNodesAtLevel() {
        tree.countNodesAtLevel(root, 0, 4);
        Map<Integer, LinkedList<BinaryTree.Node<Integer>>> depthToNodesMap = tree.getDepthToNodesMap();
        assertEquals(1, depthToNodesMap.get(0).size());
        assertEquals(2, depthToNodesMap.get(1).size());
        assertEquals(3, depthToNodesMap.get(2).size());
        assertEquals(5, depthToNodesMap.get(3).size());

    }
}