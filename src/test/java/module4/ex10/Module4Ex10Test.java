package module4.ex10;

import module4.ex3.BinaryTree;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Module4Ex10Test {
    private BinaryTree.Node<Integer> rootOuter;
    private BinaryTree treeOuter;

    private BinaryTree.Node<Integer> rootInner;
    private BinaryTree treeInner;
    private BinaryTree.Node<Integer> node9in;
    private BinaryTree.Node<Integer> node4in;
    private BinaryTree.Node<Integer> node5in;
    private BinaryTree.Node<Integer> node8in;


    @Before
    public void setUp() throws Exception {

        rootOuter = new BinaryTree.Node<>(0);
        BinaryTree.Node<Integer> node1 = new BinaryTree.Node<>(1);
        BinaryTree.Node<Integer> node2 = new BinaryTree.Node<>(2);
        BinaryTree.Node<Integer> node3 = new BinaryTree.Node<>(3);
        BinaryTree.Node<Integer> node4 = new BinaryTree.Node<>(4);
        BinaryTree.Node<Integer> node5 = new BinaryTree.Node<>(5);
        BinaryTree.Node<Integer> node6 = new BinaryTree.Node<>(6);
        BinaryTree.Node<Integer> node7 = new BinaryTree.Node<>(7);
        BinaryTree.Node<Integer> node8 = new BinaryTree.Node<>(8);
        BinaryTree.Node<Integer> node9 = new BinaryTree.Node<>(9);

        rootOuter.setLeft(node1);
        rootOuter.setRight(node2);

        node1.setLeft(node3);

        node2.setLeft(node4);
        node2.setRight(node5);

        node3.setLeft(node6);
        node3.setRight(node7);

        node4.setRight(node8);

        node5.setLeft(node9);

        List<BinaryTree.Node> nodes = new ArrayList<>();
        nodes.add(node1);
        nodes.add(node2);
        nodes.add(node3);
        nodes.add(node4);
        nodes.add(node5);
        nodes.add(node6);
        nodes.add(node7);
        nodes.add(node8);
        nodes.add(node9);
        treeOuter = new BinaryTree(nodes);
        //Inner tree
        rootInner = new BinaryTree.Node<>(2);
        node4in = new BinaryTree.Node<>(4);
        node5in = new BinaryTree.Node<>(5);
        node8in = new BinaryTree.Node<>(8);
        node9in = new BinaryTree.Node<>(9);

        rootInner.setLeft(node4);
        rootInner.setRight(node5in);
        node4in.setRight(node8in);
        node5in.setLeft(node9in);


        List<BinaryTree.Node> nodesInner = new ArrayList<>();
        nodesInner.add(node4in);
        nodesInner.add(node5in);
        nodesInner.add(node8in);
        nodesInner.add(node9in);
        nodesInner.add(rootInner);
        treeOuter = new BinaryTree(nodesInner);
    }

    @Test
    public void isSubtree() {
        assertTrue(Module4Ex10.isSubtree(rootOuter, rootInner));
        rootInner.setLeft(node5in);
        rootInner.setRight(node4in);
        assertFalse((Module4Ex10.isSubtree(rootOuter, rootInner)));
    }
}