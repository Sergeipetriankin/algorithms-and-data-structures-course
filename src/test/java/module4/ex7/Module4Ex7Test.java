package module4.ex7;

import module4.implementations.DiGraph;
import org.junit.Before;
import org.junit.Test;

public class Module4Ex7Test {

    DiGraph<String> diGraph;
    DiGraph<String> diGraphImpossible;

    @Before
    public void setUp() throws Exception {
        diGraph = new DiGraph<>();

        DiGraph.Node<String> a = new DiGraph.Node<>("a");
        DiGraph.Node<String> b = new DiGraph.Node<>("b");
        DiGraph.Node<String> c = new DiGraph.Node<>("c");
        DiGraph.Node<String> d = new DiGraph.Node<>("d");
        DiGraph.Node<String> e = new DiGraph.Node<>("e");
        DiGraph.Node<String> f = new DiGraph.Node<>("f");

        diGraph.addNode(diGraph, a);
        diGraph.addNode(diGraph, b);
        diGraph.addNode(diGraph, c);
        diGraph.addNode(diGraph, d);
        diGraph.addNode(diGraph, e);
        diGraph.addNode(diGraph, f);

        diGraph.addEdges(diGraph, f, a);
        diGraph.addEdges(diGraph, f, b);
        diGraph.addEdges(diGraph, a, d);
        diGraph.addEdges(diGraph, b, d);
        diGraph.addEdges(diGraph, d, c);

        diGraphImpossible = new DiGraph<>();

        DiGraph.Node<String> k = new DiGraph.Node<>("k");
        DiGraph.Node<String> l = new DiGraph.Node<>("l");
        DiGraph.Node<String> m = new DiGraph.Node<>("m");
        DiGraph.Node<String> n = new DiGraph.Node<>("n");
        DiGraph.Node<String> o = new DiGraph.Node<>("o");
        DiGraph.Node<String> p = new DiGraph.Node<>("p");

        diGraphImpossible.addNode(diGraphImpossible, k);
        diGraphImpossible.addNode(diGraphImpossible, l);
        diGraphImpossible.addNode(diGraphImpossible, m);
        diGraphImpossible.addNode(diGraphImpossible, n);
        diGraphImpossible.addNode(diGraphImpossible, o);
        diGraphImpossible.addNode(diGraphImpossible, p);

        diGraphImpossible.addEdges(diGraphImpossible, p, k);
        diGraphImpossible.addEdges(diGraphImpossible, p, l);
        diGraphImpossible.addEdges(diGraphImpossible, k, n);
        diGraphImpossible.addEdges(diGraphImpossible, l, n);
        diGraphImpossible.addEdges(diGraphImpossible, n, m);
        diGraphImpossible.addEdges(diGraphImpossible, m, p);

    }

    @Test
    public void projectDependencies() {
        String s = Module4Ex7.projecDependencies(diGraph);
        System.out.println(s);
        String s1 = Module4Ex7.projecDependencies(diGraphImpossible);
        System.out.println(s1);
    }
}