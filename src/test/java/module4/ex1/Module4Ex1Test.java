package module4.ex1;

import module4.implementations.Graph;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Module4Ex1Test {

    private Graph graph;
    private Graph.Node node1;
    private Graph.Node node2;
    private Graph.Node node3;
    private Graph.Node node4;
    private Graph.Node node5;

    @Before
    public void setUp() throws Exception {
        graph = new Graph();

        node1 = new Graph.Node("node1");
        node2 = new Graph.Node("node2");
        node3 = new Graph.Node("node3");
        node4 = new Graph.Node("node4");
        node5 = new Graph.Node("node5");

        node1.addChild(node2);
        node1.addChild(node3);

        node2.addChild(node1);
        node2.addChild(node3);

        node3.addChild(node1);
        node3.addChild(node2);

        node4.addChild(node5);
        node5.addChild(node4);

        graph.addNode(node1);
        graph.addNode(node2);
        graph.addNode(node3);
        graph.addNode(node4);
        graph.addNode(node5);
    }

    @Test
    public void isTherePath() {
        assertTrue(Module4Ex1.isTherePath(graph, node1, node2));
        assertTrue(Module4Ex1.isTherePath(graph, node3, node2));
        assertTrue(Module4Ex1.isTherePath(graph, node4, node5));
        assertFalse(Module4Ex1.isTherePath(graph, node2, node4));
    }
}