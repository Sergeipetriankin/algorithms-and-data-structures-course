package module2.ex6;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Module2Ex6Test {
    LinkedList<String> palindrome;
    LinkedList<String> notPalindrome;

    @Before
    public void setUp() throws Exception {
        palindrome = new LinkedList<String>();
        palindrome.addFirst("a");
        palindrome.addFirst("b");
        palindrome.addFirst("c");
        palindrome.addFirst("b");
        palindrome.addFirst("a");

        notPalindrome = new LinkedList<String>();
        notPalindrome.addFirst("a");
        notPalindrome.addFirst("b");
        notPalindrome.addFirst("c");
        notPalindrome.addFirst("d");
        notPalindrome.addFirst("e");



    }

    @Test
    public void isPailndrome() {
        assertTrue(Module2Ex6.isPailndrome(palindrome));
        assertFalse(Module2Ex6.isPailndrome(notPalindrome));
    }
}