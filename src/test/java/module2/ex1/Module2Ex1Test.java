package module2.ex1;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class Module2Ex1Test {
    private LinkedList<String> strings;
    private LinkedList<String> result;

    @Before
    public void setUp() throws Exception {
        strings = new LinkedList<String>();
        strings.add("foo");
        strings.add("bar");
        strings.add("foo");
        strings.add("bar");
        strings.add("baz");

        result = new LinkedList<String>();
        result.add("foo");
        result.add("bar");
        result.add("baz");

    }

    @Test
    public void removeDuplicates() {
        Module2Ex1.removeDuplicates(strings);
        assertEquals(strings, result);
    }
}