package module2.ex4;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class Module2Ex4Test {
    private LinkedList<Integer> list;
    @Before
    public void setUp()  {
        list = new LinkedList<Integer>();
        list.add(3);
        list.add(5);
        list.add(8);
        list.add(5);
        list.add(10);
        list.add(2);
        list.add(1);

    }

    @Test
    public void splitLinkedListByValue() {
        LinkedList<Integer> splittedList = Module2Ex4.splitLinkedListByValue(list, 5);
        List<Integer> moreThanValueSublist = new LinkedList<Integer>();
        for (int i : splittedList) {
            if (i >= 5) {
                moreThanValueSublist = splittedList.subList(splittedList.indexOf(5), splittedList.size());
                break;
            }
        }
        for (int i : moreThanValueSublist) {
            assertTrue(i >= 5);
        }
    }
}