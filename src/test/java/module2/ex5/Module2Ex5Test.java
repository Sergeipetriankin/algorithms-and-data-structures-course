package module2.ex5;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

public class Module2Ex5Test {
    LinkedList<Integer> first;
    LinkedList<Integer> second;
    LinkedList<Integer> testSum;

    @Before
    public void setUp() throws Exception {
        first = new LinkedList<Integer>();
        first.addFirst(6);
        first.addFirst(1);
        first.addFirst(7);

        second = new LinkedList<Integer>();
        second.addFirst(2);
        second.addFirst(9);
        second.addFirst(5);

        testSum = new LinkedList<Integer>();
        testSum.addFirst(9);
        testSum.addFirst(1);
        testSum.addFirst(2);
    }

    @Test
    public void sumTwoNumsAsLinkedLists() {
        LinkedList<Integer> sum = Module2Ex5.sumTwoNumsAsLinkedLists(first, second);
        assertEquals(sum, testSum);
    }
}