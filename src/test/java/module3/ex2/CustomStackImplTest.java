package module3.ex2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomStackImplTest {
    CustomStackImpl<Integer> customStack;

    @Before
    public void setUp() throws Exception {
        customStack = new CustomStackImpl<Integer>();
        customStack.push(1);
        customStack.push(5);
        customStack.push(3);
        customStack.push(4);
        customStack.push(2);
    }

    @Test
    public void min() {
        assertEquals(1, (int)customStack.min());
        customStack.push(0);
        assertEquals(0, (int)customStack.min());
        customStack.pop();
        assertEquals(1, (int)customStack.min());
    }
}