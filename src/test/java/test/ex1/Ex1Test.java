package test.ex1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Ex1Test {

    @Test
    public void isFibonacciSequence() {
        assertTrue(Ex1.isFibonacciSequence("13213455"));
        assertTrue(Ex1.isFibonacciSequence("11235813"));
        assertFalse(Ex1.isFibonacciSequence("13213454"));

    }
}