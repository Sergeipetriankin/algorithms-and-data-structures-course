package module1.ex4;

import org.junit.Test;

import static org.junit.Assert.*;

public class Module1Ex4Test {

    @Test
    public void isPalindromeReshuffle() {
        assertTrue(Module1Ex4.isPalindromeReshuffle("taco cat"));
        assertTrue(Module1Ex4.isPalindromeReshuffle("atco cta"));
        assertFalse(Module1Ex4.isPalindromeReshuffle("taco cta"));
    }
}