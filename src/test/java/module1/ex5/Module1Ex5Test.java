package module1.ex5;

import org.junit.Test;

import static org.junit.Assert.*;

public class Module1Ex5Test {

    @Test
    public void isOneStepModified() {
        assertTrue(Module1Ex5.isOneStepModified("pale", "ple"));
        assertTrue(Module1Ex5.isOneStepModified("pales", "pale"));
        assertTrue(Module1Ex5.isOneStepModified("pale", "bale"));
        assertFalse(Module1Ex5.isOneStepModified("pale", "bake"));
    }
}