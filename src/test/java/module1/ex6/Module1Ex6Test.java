package module1.ex6;

import org.junit.Test;

import static org.junit.Assert.*;

public class Module1Ex6Test {

    @Test
    public void compressString() {
        assertEquals("a2b2c5a3", Module1Ex6.compressString("aabbcccccaaa"));
        assertEquals("a1", Module1Ex6.compressString("a"));
        assertEquals("a1b1", Module1Ex6.compressString("ab"));

    }
}