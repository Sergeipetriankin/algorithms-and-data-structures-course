package module1.ex1;

import org.junit.Test;

import static org.junit.Assert.*;

public class Module1Ex1Test {

    @Test
    public void isOncePerString() {
        assertTrue(Module1Ex1.isOncePerString("qwertyuiop"));
        assertFalse(Module1Ex1.isOncePerString("qwertyuiopqawertyuiop"));

    }

    @Test
    public void isOncePerStringUsingSet() {
    }
}