package module1.ex2;

import org.junit.Test;

import static org.junit.Assert.*;

public class Module1Ex2Test {

    @Test
    public void isReshuffle() {
        assertTrue(Module1Ex2.isReshuffle("abcde", "bcdea"));
        assertTrue(Module1Ex2.isReshuffle("qwertyuiop", "poiuytrewq"));
        assertFalse(Module1Ex2.isReshuffle("abcde", "bcdeaabc"));
        assertFalse(Module1Ex2.isReshuffle("qwerty", "asdfg"));
    }
}