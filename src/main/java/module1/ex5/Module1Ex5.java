package module1.ex5;

/**
 * 40 mins
 */
public class Module1Ex5 {
    public static boolean isOneStepModified(String target, String toCheck) {
        if (target == null || toCheck == null) {
            return false;
        }
        char[] targetChars = target.toCharArray();
        char[] toCheckChars = toCheck.toCharArray();
        int targetLength = targetChars.length;
        int toCheckLength = toCheckChars.length;
        int distance = Math.abs(targetLength - toCheckLength);
        if (distance > 1) {
            return false;
        }
        int count = 0;
        if (distance == 1) {
            for (int i = 0; i < Math.min(targetLength, toCheckLength); i++) {
                if (targetChars[i] != toCheckChars[i]) {
                    count++;
                }
            }
            return count == 0;
        }
        return false;
    }
}
