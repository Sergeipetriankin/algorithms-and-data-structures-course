package module1.ex6;

/**
 * Appx time 50 mins
 */
public class Module1Ex6 {
    // O(n) ?
    public static String compressString(String input) {
        if (input == null) {
            return null;
        }
        if (input.equals("")) return "";
        StringBuilder result = new StringBuilder("");
        char[] chars = input.toCharArray();
        int counter = 1;
        result.append(chars[0]);
        for (int i = 1; i < chars.length; i++) {
            if (chars[i] == chars[i - 1]) counter++;
            else {
                result.append(counter);
                result.append(chars[i]);
                counter = 1;
            }
        }
        result.append(counter);
        return result.toString();
    }
}
