package module1.ex2;

import java.util.Arrays;

/**
 * Appx time 20 mins
 */
public class Module1Ex2 {
    //O(nlogn) ?
    public static boolean isReshuffle(String target, String toCheck) {
        char[] targetStringChars = target.toCharArray();
        char[] toCheckStringChars = toCheck.toCharArray();
        Arrays.sort(targetStringChars);
        Arrays.sort(toCheckStringChars);
        return Arrays.equals(targetStringChars, toCheckStringChars);

    }

}
