package module1.ex4;

/**
 * Appxt time 10 mins
 */
public class Module1Ex4 {
    //O(n)
    public static boolean isPalindromeReshuffle(String toCheck) {
        char[] chars = toCheck.replaceAll("\\s", "").toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != chars[chars.length - 1 - i]) {
                return false;
            }
        }
        return true;
    }
}
