package module1.ex1;

import java.util.HashSet;
import java.util.Set;

/**
 * Appx time 20 mins
 */
public class Module1Ex1 {

    // O(n^2) ?
    public static boolean isOncePerString(String input) {
        char[] chars = input.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            for(int j = 0; j < chars.length; j++) {
                if (i != j && chars[i] == chars[j]) {
                    return false;
                }
            }
        }
        return true;
    }

    //O(n) ?
    public static boolean isOncePerStringUsingSet(String string) {
        Set<Character> characters = new HashSet<Character>();
        char[] chars = string.toCharArray();
        for (char c : chars) {
            characters.add(c);
        }
        return chars.length == characters.size();
    }
}
