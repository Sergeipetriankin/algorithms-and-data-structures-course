package module2.ex6;

import java.util.LinkedList;

/**
 * Appx time 15 mins
 */

public class Module2Ex6 {
    //O(n) ?
    public static <T> boolean isPailndrome(LinkedList<T> list) {
        boolean result = true;
        for (int i = 0; i < list.size() / 2; i++) {
            if (!list.getFirst().equals(list.getLast())){
                result = false;
            }
        }
        return result;
    }
}
