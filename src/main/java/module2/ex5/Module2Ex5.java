package module2.ex5;

import java.util.LinkedList;

/**
 * Appxt time 70 mins
 */
public class Module2Ex5 {
    // TODO: 12/6/2017 implement for the case when digits are not the same size
    public static LinkedList<Integer> sumTwoNumsAsLinkedLists(LinkedList<Integer> first, LinkedList<Integer> second) {
        LinkedList<Integer> result = new LinkedList<Integer>();
        int digits = Math.min(first.size(), second.size());
        int addition = 0;
        for (int i = 0; i < digits; i++) {
            int firstDigit = first.get(i);
            int secondDigit = second.get(i);
            int sum = firstDigit + secondDigit + addition;
            if (sum > 9) {
                sum = sum % 10;
                addition = 1;
            } else {
                addition = 0;
            }
            result.addLast(sum);
        }
        return result;
    }
}
