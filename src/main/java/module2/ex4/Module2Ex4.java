package module2.ex4;

import java.util.LinkedList;

/**
 * Appxt time 15 mins
 */
public class Module2Ex4 {
    //O(n)
    public static LinkedList<Integer> splitLinkedListByValue(LinkedList<Integer> inputList, int value) {
        LinkedList<Integer> result = new LinkedList<Integer>();
        for (Integer i : inputList) {
            if (i >= value) {
                result.addLast(i);
            }
            else {
                result.addFirst(i);
            }
        }
        return result;
    }
}
