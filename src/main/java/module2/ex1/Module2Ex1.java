package module2.ex1;

import java.util.*;

/**
 * Appxt time 20 mins
 */
public class Module2Ex1 {
    // O(n) ?
    public static void removeDuplicates(LinkedList objectLinkedList) {
        Iterator iterator = objectLinkedList.iterator();
        Set objects = new LinkedHashSet<Object>();
        while (iterator.hasNext()) {
            objects.add(iterator.next());
        }
        objectLinkedList.clear();
        for (Object o : objects) {
            objectLinkedList.add(o);
        }

    }
}
