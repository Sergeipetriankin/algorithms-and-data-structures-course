package module8.ex7;

import java.util.ArrayList;
import java.util.List;

/**
 * Appxt time 60 mins
 */
public class Module8Ex7 {
    /*                                      abc

                            bac             cab             abc
                        /       \         /      \        /      \
                      bac       bca     cba     cab      acb    abc


     */

    // O(n) where n is the length of the string
    public static List<String> permutation(String input) {
        permutation("", input);
        return result;
    }

    private static List<String> result = new ArrayList<>();

    private static void permutation(String fixed, String input) {
        int length = input.length();
        if (length == 0) {
            result.add(fixed);
        } else {
            for (int i = 0; i < length; i++) {
                permutation(fixed + input.charAt(i), input.substring(0, i) + input.substring(i + 1, length));
            }
        }
    }
}
