package module8.ex2;

public class Robot {
    private int currentX;
    private int currentY;

    public Robot(int currentX, int currentY) {
        this.currentX = currentX;
        this.currentY = currentY;
    }

    public void moveRight() {
        currentX++;
    }

    public void moveDown() {
        currentY++;
    }

    public int getCurrentX() {
        return currentX;
    }

    public int getCurrentY() {
        return currentY;
    }
}
