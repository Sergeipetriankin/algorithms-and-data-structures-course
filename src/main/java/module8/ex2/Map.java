package module8.ex2;

public class Map {
    private int length;
    private int height;
    private int[][] map;


    public Map(int[][] map) {
        this.map = map;
    }

    public Map(int length, int height) {
        this.length = length;
        this.height = height;
        this.map = new int[length][height];
    }

    public int getLength() {
        return length;
    }

    public int getHeight() {
        return height;
    }
}
