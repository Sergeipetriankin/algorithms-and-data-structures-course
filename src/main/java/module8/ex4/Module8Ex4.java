package module8.ex4;

/**
 * Appxt time 60 mins
 */
public class Module8Ex4 {
    public static void allSubsetsOfSet(char[] set){
        int N = set.length;
        for (int mask = 0; mask < (1 << N); mask++) {
            for (int j = 0; j < N; j++) {
                if((mask & (1 << j)) != 0){
                    System.out.print(set[j] + " ");
                }
            }
            System.out.println();
        }
    }
}
