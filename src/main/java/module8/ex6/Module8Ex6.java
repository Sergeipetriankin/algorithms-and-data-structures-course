package module8.ex6;

import java.util.Stack;

/**
 * Appxt time 30 mins
 */
public class Module8Ex6 {

    // O(2^n)
    public static void towerOfHanoi(int disksAmount, Stack<Integer> source, Stack<Integer> destination, Stack<Integer> temp) {
        if (disksAmount == 1) {
            destination.push(source.pop());
            return;
        }
        towerOfHanoi(disksAmount - 1, source, temp, destination);
        destination.push(source.pop());
        towerOfHanoi(disksAmount - 1, temp, destination, source);
    }
}
