package module3.ex2;

import java.util.EmptyStackException;

/**
 * Appxt time 60 mins
 *
 * @param <T>
 */
public class CustomStackImpl<T extends Comparable<T>> {
    private static class StackNode<T> {
        private T data;
        private T minData;
        private StackNode<T> next;

        public StackNode(T data) {
            this.data = data;
            minData = data;
        }
    }

    private StackNode<T> top;

    public T pop() {
        if (top == null) {
            throw new EmptyStackException();
        }
        T item = top.data;
        top = top.next;
        return item;
    }

    public void push(T item) {
        StackNode<T> node = new StackNode<T>(item);
        node.next = top;
        top = node;
        if (top.next != null) {
            if (top.minData.compareTo(top.next.minData) > 0) {
                top.minData = top.next.minData;
            }
        } else {
            top.minData = top.data;
        }
    }

    public T peek() {
        if (top == null) {
            throw new EmptyStackException();
        }
        return top.data;
    }

    public T min() {
        return top.minData;
    }

    public boolean isEmpty() {
        return top == null;
    }


}
