package module4.ex7;

import module4.implementations.DiGraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Appx time 180 mins
 */
public class Module4Ex7 {

    private static HashMap<DiGraph.Node<String>, Integer> nodeWithDependenciesMap = new HashMap<>();

    // O(n^2 ?)
    public static String projecDependencies(DiGraph<String> stringDiGraph) {
        StringBuilder sb = new StringBuilder();
        HashSet<DiGraph.Node<String>> nodes = stringDiGraph.getNodes();

        // putting all nodes with their dependencies
        for (DiGraph.Node node : nodes) {
            nodeWithDependenciesMap.put(node,
                    node
                            .getParents()
                            .size());
        }

        while (nodeWithDependenciesMap.size() > 0) {
            boolean isPossible = false;
            for (Map.Entry<DiGraph.Node<String>, Integer> pair : nodeWithDependenciesMap.entrySet()) {
                if (pair.getValue() == 0) {
                    isPossible = true;
                    sb.append(pair.getKey().getValue());
                    sb.append(", ");
                    stringDiGraph.removeNode(stringDiGraph, pair.getKey());
                    nodeWithDependenciesMap.remove(pair.getKey());
                    for (DiGraph.Node<String> child : pair.getKey().getChildren()) {
                        nodeWithDependenciesMap.put(child, nodeWithDependenciesMap.get(child) - 1);
                    }
                    break;
                }
            }
            if (!isPossible) {
                sb.append("There seems to be cyclic dependency between some of the nodes");
                break;
            }

        }
        return sb.toString();


    }


}
