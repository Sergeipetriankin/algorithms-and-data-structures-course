package module4.implementations;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

public class DiGraph<T> {
    private HashSet<Node<T>> nodes = new HashSet<>();
    private HashSet<Edge> edges = new HashSet<>();
    private HashMap<T, Node> nodeMap = new HashMap<>();

    public static class Node<T> {
        T t;
        HashSet<Edge> edges;
        HashSet<Node> parents;

        public HashSet<Node> getChildren() {
            return children;
        }

        HashSet<Node> children;


        public Node(T t) {
            this.t = t;
            edges = new HashSet<>();
            parents = new HashSet<>();
            children = new HashSet<>();
        }

        public HashSet<Node> getParents() {
            return parents;
        }

        public T getValue() {
            return t;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "t=" + t +
                    '}';
        }
    }

    public static class Edge {
        Node startingNode;
        Node finishNode;


        public Edge(Node startingNode, Node finishNode) {
            this.startingNode = startingNode;
            this.finishNode = finishNode;
        }

        public Node getStartingNode() {
            return startingNode;
        }

        public Node getFinishNode() {
            return finishNode;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Edge edge = (Edge) o;
            return Objects.equals(startingNode, edge.startingNode) &&
                    Objects.equals(finishNode, edge.finishNode);
        }

        @Override
        public int hashCode() {
            return Objects.hash(startingNode, finishNode);
        }

    }

    public void addNode(DiGraph<T> graph, Node<T> nodeToAdd) {
        graph.nodes.add(nodeToAdd);
        graph.nodeMap.put(nodeToAdd.t, nodeToAdd);
    }

    public void removeNode(DiGraph<T> graph, Node<T> nodeToRemove) {
        Iterator<Edge> iterator = graph.edges.iterator();

        while (iterator.hasNext()) {
            Edge next = iterator.next();
            if (nodeToRemove.equals(next.startingNode) || nodeToRemove.equals(next.finishNode)) {
                iterator.remove();
            }
        }
        graph.nodes.remove(nodeToRemove);
        graph.nodeMap.remove(nodeToRemove.t);
    }

    public void addEdges(DiGraph<T> graph, Node<T> startingNode, Node<T> finishingNode) {
        Edge edge = new Edge(startingNode, finishingNode);
        graph.edges.add(edge);
        startingNode.edges.add(edge);
        nodes.add(startingNode);
        finishingNode.parents.add(startingNode);
        startingNode.children.add(finishingNode);
    }


    public Node getNode(T t) {
        if (nodeMap.containsKey(t)) {
            return nodeMap.get(t);
        }
        return null;
    }

    public HashSet<Node<T>> getNodes() {
        return nodes;
    }

    public HashSet<Edge> getEdges() {
        return edges;
    }

    public HashMap<T, Node> getNodeMap() {
        return nodeMap;
    }
}
