package module4.implementations;

import java.util.ArrayList;
import java.util.List;

public class Graph {
    private List<Node> nodes;

    public void addNode(Node node) {
        nodes.add(node);
    }
    public List<Node> getNodes() {
        return nodes;
    }

    public Graph() {
        nodes = new ArrayList<>();
    }

    public static class Node {
        private String name;
        private boolean isVisited;
        private List<Node> children = new ArrayList<>();

        public Node(String name) {
            this.name = name;
        }

        public void addChild(Node node) {
            List<Node> children = getChildren();
            children.add(node);
            setChildren(children);
        }

        public boolean isVisited() {
            return isVisited;
        }

        public void setVisited(boolean visited) {
            isVisited = visited;
        }

        public List<Node> getChildren() {
            return children;
        }

        public void setChildren(List<Node> children) {
            this.children = children;
        }

    }

}
