package module4.ex1;

import module4.implementations.Graph;

import java.util.LinkedList;


/**
 * Appxt time 180 mins
 */
public class Module4Ex1 {

    public static boolean isTherePath(Graph graph, Graph.Node fromNode, Graph.Node toNode) {
        LinkedList<Graph.Node> nodes = new LinkedList<>();
        nodes.add(fromNode);
        Graph.Node currentNode;
        while (!nodes.isEmpty()) {
            currentNode = nodes.removeFirst();
            for (Graph.Node node : graph.getNodes()) {
                if (!node.isVisited()) {
                    if (node.equals(toNode)) {
                        return true;
                    } else {
                        nodes.add(node);
                    }
                }
                currentNode.setVisited(true);
            }
        }


        return false;
    }
}