package module4.ex10;

import module4.ex3.BinaryTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Appxt time 120 mins
 */
public class Module4Ex10 {

    //O(nm) where n and m are trees depths
    public static boolean isSubtree(module4.ex3.BinaryTree.Node outerTreeRoot, module4.ex3.BinaryTree.Node innerTreeRoot) {
        boolean inOrderIdentity = Collections.indexOfSubList(inOrderTraversal(outerTreeRoot), inOrderTraversal(innerTreeRoot)) != -1;
        boolean preOrderIdentity = Collections.indexOfSubList(preOrderTraversal(outerTreeRoot), preOrderTraversal(innerTreeRoot)) != -1;
        return inOrderIdentity && preOrderIdentity;
    }

    public static ArrayList<module4.ex3.BinaryTree.Node> inOrderTraversal(module4.ex3.BinaryTree.Node root) {
        ArrayList<BinaryTree.Node> inOrderTraversalNodes = new ArrayList<>();
        if (root != null) {
            inOrderTraversalNodes.addAll(inOrderTraversal(root.getLeft()));
            inOrderTraversalNodes.add(root);
            inOrderTraversalNodes.addAll(inOrderTraversal(root.getRight()));
        }
        return inOrderTraversalNodes;
    }

    public static ArrayList<module4.ex3.BinaryTree.Node> preOrderTraversal(module4.ex3.BinaryTree.Node root) {
        ArrayList<module4.ex3.BinaryTree.Node> preOrderTraversalNodes = new ArrayList<>();
        if (root != null) {
            preOrderTraversalNodes.add(root);
            preOrderTraversalNodes.addAll(preOrderTraversal(root.getLeft()));
            preOrderTraversalNodes.addAll(preOrderTraversal(root.getRight()));
        }
        return preOrderTraversalNodes;
    }
}
