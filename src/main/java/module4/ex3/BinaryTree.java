package module4.ex3;

import java.util.*;

/**
 * Appxt 120 mins
 */
public class BinaryTree<T> {

    private Node<T> root;
    private List<Node> nodes;
    private Map<Integer, LinkedList<Node>> depthToNodesMap = new HashMap<>();

    public BinaryTree(List<Node> nodes) {
        this.nodes = nodes;
    }

    public static class Node<T> {
        T data;
        Node left;
        Node right;

        public Node(T data) {
            this.data = data;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public void setRight(Node right) {
            this.right = right;
        }

        public Node getLeft() {
            return left;
        }

        public Node getRight() {
            return right;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return Objects.equals(data, node.data);
        }

        @Override
        public int hashCode() {

            return Objects.hash(data);
        }

        @Override
        public String toString() {
            return "" + data;
        }
    }

    public Node<T> getRoot() {
        return root;
    }

    public Map<Integer, LinkedList<Node>> getDepthToNodesMap() {
        return depthToNodesMap;
    }

    public  int countNodesAtLevel(BinaryTree.Node node, int currentLevel, int targetLevel) {
        if (node == null) {
            return 0;
        }
        if (currentLevel == targetLevel) {
            return 1;
        }

        if (depthToNodesMap.containsKey(currentLevel)) {
            LinkedList<Node> nodes = depthToNodesMap.get(currentLevel);
            nodes.add(node);
        } else {
            LinkedList<Node> newList = new LinkedList<>();
            newList.add(node);
            depthToNodesMap.put(currentLevel, newList);
        }
        return countNodesAtLevel(node.left, currentLevel + 1, targetLevel) +
                countNodesAtLevel(node.right, currentLevel + 1, targetLevel);


    }
}
