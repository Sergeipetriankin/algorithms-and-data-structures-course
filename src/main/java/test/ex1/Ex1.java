package test.ex1;

public class Ex1 {

    // seems to be O(n)
    public static boolean isFibonacciSequence(String input) {
        int inputInt = 0;
        try {
            inputInt = Integer.parseInt(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (input == null || input.isEmpty()) {
            return false;
        }
        StringBuilder sb = new StringBuilder();

        //generating no more than input fib number
        for (int i = 1; i< inputInt ; i++) {
            int nthFibo = nthFibo(i);
            if (nthFibo < 1) {
                return false;
            }
            if (nthFibo > inputInt) {
                break;
            }
            sb.append(nthFibo);
            if (sb.toString().contains(input)) {
                return true;
            }
        }
        return false;
    }


    private static int nthFibo(int n) {
        if (n < 0) {
            return -1;
        }
        if (n == 0) {
            return -1;
        }
        int a = 1;
        int b = 1;
        for (int i = 3; i <= n; i++) {
            int c = a + b;
            a = b;
            b = c;
        }
        return b;
    }

}
