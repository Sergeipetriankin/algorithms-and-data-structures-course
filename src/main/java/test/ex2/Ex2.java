package test.ex2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Ex2 {

    private static List<Integer> reshuffles = new ArrayList<Integer>();
    static void reshuffle(List<Integer> array, int k) {
        for (int i = k; i < array.size(); i++) {
            Collections.swap(array, i, k);
            reshuffle(array, k + 1);
            Collections.swap(array, k, i);
        }
        if (k == array.size() - 1) {
            StringBuilder sb = new StringBuilder();
            for (int i : array) {
                sb.append(i);
            }
            reshuffles.add(Integer.parseInt(sb.toString()));
        }
    }

    public static void main(String[] args) {
        Ex2.reshuffle(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9), 0);
        Collections.sort(reshuffles);
        List<Integer> leftHalf = reshuffles.subList(0, reshuffles.size() / 2);
        List<Integer> rightHalf = reshuffles.subList(reshuffles.size() / 2, reshuffles.size() - 1);

        // at least O(n2)
        for (int i : rightHalf) {
            for (int j : leftHalf) {
                if (i % j == 0) {
                    System.out.println(j + " , " + i);
                }
            }
        }


    }
}